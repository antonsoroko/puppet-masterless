class quote-web {
  class { 'docker':
    manage_kernel => false,
    docker_users => ['ubuntu'],
  }
  docker::image { 'ubuntu':
    image_tag => 'xenial'
  }
  docker::run { 'nginx':
    image   => 'nginx',
    ports  => ['8080:80'],
    env_file => ['/srv/quote-web.env'],
    restart_service => true,
    extra_parameters => [ '--restart=always' ],
  }
}
